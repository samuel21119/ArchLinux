Arch Linux 快速安裝指南
=====

驗證開機模式、磁碟分割
-----
### 驗證是否為UEFI開機模式:  

	# ls /sys/firmware/efi/efivars
### 顯示硬碟分割區

	# fdisk -l 
### 分割硬碟

	# gdisk /dev/sdx

格式化、裝載磁碟
-----
### 格式化:

    # mkfs.ext4 [root partition]
    # mkfs.fat -F 32 [efi boot partition]
### 裝載磁碟

    # mount [root partition] /mnt
    # mkdir /mnt/boot
    # mount [efi boot partition] /mnt/boot
安裝作業系統、磁碟裝載檔案
----

    # vim /etc/pacman.d/mirrorlist
    (更改mirror檔案，加快下載)
    
	# pacstrap /mnt base base-devel grub sudo vim
	
	# genfstab -U /mnt >> /mnt/etc/fstab
    (use -U or -L to define by UUID or labels, respectively)
設定系統
----

	# arch-chroot /mnt
	(切換root)
	
	[root@arch /]# ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime
    (設定timezone)
      
	[root@arch /]# vim /etc/locale.gen
	[root@arch /]# locale-gen
	[root@arch /]# echo LANG=en_US.UTF-8 > /etc/locale.conf
    (設定語言)
    
	[root@arch /]# echo [hostname] > /etc/hostname
    (設定電腦名稱)
    
	[root@arch /]# passwd
	[root@arch /]# useradd -mg users -G wheel,storage,power -s /bin/bash [user name]
	[root@arch /]# vim /etc/sudoers
	(更改root密碼、新增使用者、且在/etc/sudoers裡面取消註解%wheel ALL該行)
安裝Bootloader
----


	[root@arch /]# pacman -S grub efibootmgr os-prober
	[root@arch /]# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
	[root@arch /]# grub-mkconfig -o /boot/grub/grub.cfg
	
結束
----
	[root@arch /]# exit
	
	# umount -R /mnt
	# reboot

額外設定
----
### yaourt 

    # git clone https://aur.archlinux.org/package-query.git
    # cd package-query
    # makepkg -si --noconfirm
    # git clone https://aur.archlinux.org/yaourt.git
    # cd yaourt
    # makepkg -si --noconfirm
    # cd ~
    # rm -rf package-query

	
### Gnome

	# pacman -S xorg
	# pacman -S gnome
	# systemctl [enable/start] gdm
	
### Internet

##### wifi-menu
	# pacman -S wpa_supplicant dialog
	
##### Auto connect to wifi

	# netctl enable [SSID]
	
##### ethernet
	# systemctl enable dhcpcd

### 中文輸入

    # sudo pacman -S fcitx-im fcitx-chewing fcitx-configtool
    # sudo vim /etc/environment
加入以下3行

    GTK_IM_MODULE=fcitx
    QT_IM_MODULE=fcitx
    XMODIFIERS="@im=fcitx"
        